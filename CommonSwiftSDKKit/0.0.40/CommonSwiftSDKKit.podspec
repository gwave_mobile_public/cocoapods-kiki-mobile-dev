Pod::Spec.new do |spec|
    spec.name                     = 'CommonSwiftSDKKit'
    spec.version                  = '0.0.40'
    spec.homepage                 = 'https://gitlab.com/gwave_mobile_public/commonswiftsdkkitframework'
    spec.source                   = { :git=> 'https://gitlab.com/gwave_mobile_public/commonswiftsdkkitframework.git', :tag => '0.0.40'}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'factory Shared Module for dependency injection'
    spec.vendored_frameworks      = 'CommonSwiftSDKKit.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '14.0'
    spec.swift_version            = '5.0'
    spec.static_framework = true
    spec.dependency 'FingerprintJS', '1.3.0'
    spec.dependency 'Web3Auth', '~> 8.4.0'
    spec.dependency 'secp256k1.swift','0.1.4'
    spec.dependency 'web3.mugen.swift', '1.6.0'
    spec.dependency 'JOSESwift', '2.4.0'
    spec.dependency 'FirebaseDynamicLinks', '10.10.0'
    spec.dependency 'Blake2', '0.2'
                         
end