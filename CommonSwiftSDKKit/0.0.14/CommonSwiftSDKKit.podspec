Pod::Spec.new do |spec|
    spec.name                     = 'CommonSwiftSDKKit'
    spec.version                  = '0.0.14'
    spec.homepage                 = 'https://gitlab.com/gwave_mobile_public/commonswiftsdkkitframework'
    spec.source                   = { :git=> 'https://gitlab.com/gwave_mobile_public/commonswiftsdkkitframework.git', :tag => '0.0.14'}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'factory Shared Module for dependency injection'
    spec.vendored_frameworks      = 'CommonSwiftSDKKit.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '14.0'
    spec.swift_version            = '5.0'
    spec.dependency 'FingerprintJS', '1.3.0'
    spec.dependency 'Web3AuthMugen', '5.0.0'
    spec.dependency 'Firebase', '10.10.0'
    spec.dependency 'FirebaseDynamicLinks', '10.10.0'
                         
end