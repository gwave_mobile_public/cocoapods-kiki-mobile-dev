Pod::Spec.new do |spec|
    spec.name                     = 'CommonSwiftSDKKit'
    spec.version                  = '0.0.2'
    spec.homepage                 = 'https://gitlab.com/gwave_mobile_public/commonswiftsdkkitframework'
    spec.source                   = { :git=> 'https://gitlab.com/gwave_mobile_public/commonswiftsdkkitframework.git', :tag => '0.0.2'}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'factory Shared Module for dependency injection'
    spec.vendored_frameworks      = 'CommonSwiftSDKKit.framework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '14.0'
    spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
                
                
                
                
end