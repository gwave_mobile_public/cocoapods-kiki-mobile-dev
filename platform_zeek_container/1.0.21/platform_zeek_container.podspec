Pod::Spec.new do |spec|
    spec.name                     = 'platform_zeek_container'
    spec.version                  = '1.0.21'
    spec.homepage                 = 'https://gitlab.com/gwave_mobile_public/talentframework'
    spec.source                   = { :git=> 'https://gitlab.com/gwave_mobile_public/talentframework.git', :tag => '1.0.21'}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'factory Shared Module for dependency injection'
    spec.vendored_frameworks      = 'Zeek.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '14.0'
    spec.dependency 'CommonSwiftSDKKit', '0.0.32'
                
                
                
end