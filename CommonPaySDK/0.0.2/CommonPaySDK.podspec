Pod::Spec.new do |spec|
    spec.name                     = 'CommonPaySDK'
    spec.version                  = '0.0.2'
    spec.homepage                 = 'https://gitlab.com/gwave_mobile_public/commonpaysdkframework'
    spec.source                   = { :git=> 'https://gitlab.com/gwave_mobile_public/commonpaysdkframework.git', :tag => '0.0.2'}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'factory Shared Module for dependency injection'
    spec.vendored_frameworks      = 'CommonPaySDK.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '11.0'
    spec.swift_version            = '5.0'
                
                
                
                
end