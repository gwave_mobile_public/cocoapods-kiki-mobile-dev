Pod::Spec.new do |spec|
  spec.name          = "Web3AuthMugen"
  spec.version       = "5.0.0"
  spec.platform      = :ios, "14.0"
  spec.summary       = "Torus Web3AuthMugen SDK for iOS applications"
  spec.homepage      = "https://gitlab.com/gwave_mobile_public/web3auth-swift-sdk"
  spec.license       = { :type => 'MIT', :file => 'LICENSE.md' }
  spec.swift_version = "5.0"
  spec.author        = { "Torus Labs" => "hello@tor.us" }
  spec.module_name   = "Web3Auth"
  spec.source        = { :git => "https://gitlab.com/gwave_mobile_public/web3auth-swift-sdk.git", :tag => spec.version }
  spec.source_files  = "Sources/Web3Auth/*.{swift}", "Sources/Web3Auth/**/*.{swift}"
  spec.dependency 'KeychainSwift', '~> 20.0.0'
  spec.dependency 'web3Mugen.swift', '~> 0.9.3'
  spec.dependency 'CryptoSwift', '~> 1.5.1'
  spec.exclude_files = [ 'docs/**' ]
end
