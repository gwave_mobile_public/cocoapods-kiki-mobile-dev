#
# Be sure to run `pod lib lint CommonRNSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CommonRNSDK'
  s.version          = '0.0.21'
  s.summary          = 'A short description of CommonRNSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/gwave_mobile_public/commonrnsdk'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yangpeng' => 'peng.yang@kikitrade.com' }
  s.source           = { :git => 'https://gitlab.com/gwave_mobile_public/commonrnsdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

#  s.source_files = 'CommonRNSDK/Classes/**/*'
  
  s.default_subspec = 'Headers','Bundle','Rct','Views'

  s.subspec 'Headers' do |spec|
      spec.source_files = 'CommonRNSDK/Classes/Headers/**/*.{h,m}'
  end

  s.subspec 'Bundle' do |spec|
      spec.source_files = 'CommonRNSDK/Classes/Bundle/**/*.{h,m}'
  end
  
  s.subspec 'Rct' do |spec|
      spec.source_files = 'CommonRNSDK/Classes/Rct/**/*.{h,m}'
  end

  s.subspec 'Views' do |spec|
      spec.source_files = 'CommonRNSDK/Classes/Views/**/*.{h,m}'
  end

  
  
  # s.resource_bundles = {
  #   'CommonRNSDK' => ['CommonRNSDK/Assets/*.png']
  # }
  
  s.dependency 'SSZipArchive', '2.4.3'
  s.dependency 'FeatureProbe', '2.0.2'
  s.dependency 'React-Core'
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
