Pod::Spec.new do |spec|
    spec.name                     = 'platform_kiki_container'
    spec.version                  = '1.0.73'
    spec.homepage                 = 'https://gitlab.com/Keccak256-evg/gwave-mobile/kikiappframework'
    spec.source                   = { :git=> 'https://gitlab.com/gwave_mobile_public/artifacts-platform-kiki-container-dev.git', :tag => 'v1.0.73'}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'factory Shared Module for dependency injection'
    spec.vendored_frameworks      = 'KikiContainer.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '11.0'
                
                
                
                
end