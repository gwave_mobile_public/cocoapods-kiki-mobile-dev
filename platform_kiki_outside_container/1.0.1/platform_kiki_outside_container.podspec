Pod::Spec.new do |spec|
    spec.name                     = 'platform_kiki_outside_container'
    spec.version                  = '1.0.1'
    spec.homepage                 = 'https://gitlab.com/Keccak256-evg/gwave-mobile/kikiappframework'
    spec.source                   = { :git=> 'https://gitlab.com/gwave_mobile_public/MugenContainer.git', :tag => 'v1.0.1'}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'factory Shared Module for dependency injection'
    spec.vendored_frameworks      = 'MugenContainer.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '14.0'
    spec.dependency 'CommonIMSDK', '0.0.2'
    spec.dependency 'CommonPaySDK', '0.0.2'
    spec.dependency 'CommonSwiftSDKKit', '0.0.17'
                
                
                
end