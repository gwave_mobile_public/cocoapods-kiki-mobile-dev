Pod::Spec.new do |spec|
    spec.name                     = 'CommonIMSDK'
    spec.version                  = '0.0.1'
    spec.homepage                 = 'https://gitlab.com/gwave_mobile_public/commonimsdkframework'
    spec.source                   = { :git=> 'https://gitlab.com/gwave_mobile_public/commonimsdkframework.git', :tag => '0.0.1'}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'factory Shared Module for dependency injection'
    spec.vendored_frameworks      = 'CommonIMSDK.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '11.0'
    spec.swift_version            = '5.0'
    spec.dependency 'TXIMSDK_Plus_iOS', '7.2.4146'
                    
                
                
end